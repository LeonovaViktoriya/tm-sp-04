package ru.leonova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.service.ITaskService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class TaskController {

    @Autowired
    ITaskService taskService;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy");

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public String allTasks(Model model) {
        List<Task> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        return "view/taskList";
    }

    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.GET)
    public String allTasksByProject(Model model, @PathVariable("id") String id) throws Exception {
        List<Task> tasks = taskService.findAllByProjectId(id);
        model.addAttribute(id);
        model.addAttribute("tasks", tasks);
        return "view/taskList";
    }

    @RequestMapping(value = "/tasks/createForProject/{id}", method = RequestMethod.GET)
    public ModelAndView addPage(@PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id", id);
        modelAndView.setViewName("view/taskCreate");
        return modelAndView;
    }

    @RequestMapping(value = "/tasks/add/{id}", method = RequestMethod.POST)
    public String addTaskForProject(@PathVariable("id") String id, @ModelAttribute("name") String name, @ModelAttribute("description") String desc, @ModelAttribute("dateStart") String dateStart,
                                    @ModelAttribute("dateEnd") String dateEnd, @ModelAttribute("dateSystem") Date dateSystem) throws ParseException {
        Task task = new Task();
        task.setTaskId(UUID.randomUUID().toString());
        task.setDescription(desc);
        try {
            task.setDateStart(dateFormat.parse(dateStart));
            task.setDateEnd(dateFormat.parse(dateEnd));
        }catch (Exception e){
            task.setDateStart(null);
            task.setDateEnd(null);
        }
        task.setDateSystem(dateSystem);
        task.setName(name);
        task.setStatus(Status.PLANNED);
        Project project = new Project();
        project.setProjectId(id);
        task.setProject(project);
        taskService.save(task);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/tasks/delete/{id}", method = RequestMethod.GET)
    public String deleteTasks(@PathVariable("id") String id) throws Exception {
        Task task = taskService.findOneById(id);
        if (task != null) taskService.removeTask(task);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/tasks/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") String id) throws Exception {
        Task task = taskService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("view/taskCreate");
        if (task != null) modelAndView.addObject("task", task);
        return modelAndView;
    }

    @RequestMapping(value = "/tasks/edit/{id}", method = RequestMethod.POST)
    public String editTask(@PathVariable("id") String id, @ModelAttribute("name1") String name, @ModelAttribute("description1") String description,
                           @ModelAttribute("dateStart1") String dateStart,
                           @ModelAttribute("dateEnd1") String dateEnd) throws Exception {
        Task task = taskService.findOneById(id);
        if (task != null) {
            task.setDescription(description);
            task.setName(name);
            task.setDateSystem(new Date());
            try {
                task.setDateStart(dateFormat.parse(dateStart));
                task.setDateEnd(dateFormat.parse(dateEnd));
            }catch (Exception e){
                task.setDateStart(null);
                task.setDateEnd(null);
            }
            taskService.save(task);
        }
        return "redirect:/tasks";
    }
}
