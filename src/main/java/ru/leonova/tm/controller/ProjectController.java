package ru.leonova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.service.IProjectService;
import ru.leonova.tm.service.ITaskService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class ProjectController {

    @Autowired
    IProjectService projectService;

    @Autowired
    ITaskService taskService;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy");

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/view/index");
        return modelAndView;
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public String allProjects(Model model) {
        List<Project> projects = projectService.findAll();
        model.addAttribute("projects", projects);
        return "view/projectList";
    }

    @RequestMapping(value = "/projects/delete/{id}", method = RequestMethod.GET)
    public String deleteProjects(@PathVariable("id") String id) throws Exception {
        Project project = projectService.findOneById(id);
        taskService.removeAllByProjectId(id);
        projectService.removeProject(project);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/projects/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") String id) throws Exception {
        Project project = projectService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("view/updateProject");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @RequestMapping(value = "/projects/edit/{id}", method = RequestMethod.POST)
    public String editProjects(@PathVariable("id") String id, @ModelAttribute("name1") String name, @ModelAttribute("description1") String desc,
                               @ModelAttribute("dateStart") String dateStart, @ModelAttribute("dateEnd") String dateEnd) throws Exception {
        Project project = projectService.findOneById(id);
//        project.setProjectId(id);
        project.setDescription(desc);
        project.setName(name);
        project.setDateSystem(new Date());
        try {
            project.setDateStart(dateFormat.parse(dateStart));
            project.setDateEnd(dateFormat.parse(dateEnd));
        }catch (Exception e){
            project.setDateStart(null);
            project.setDateEnd(null);
        }
        projectService.save(project);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/projects/add", method = RequestMethod.GET)
    public ModelAndView addPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("view/updateProject");
        return modelAndView;
    }

    @RequestMapping(value = "/projects/add", method = RequestMethod.POST)
    public String addProject(@ModelAttribute("name") String name, @ModelAttribute("description") String desc, @ModelAttribute("dateSystem") Date dateSystem,
                             @ModelAttribute("dateStart1") String dateStart, @ModelAttribute("dateEnd1") String dateEnd) throws ParseException {
        Project project = new Project();
        project.setName(name);
        project.setDescription(desc);
        project.setDateSystem(dateSystem);
        try {
            project.setDateStart(dateFormat.parse(dateStart));
            project.setDateEnd(dateFormat.parse(dateEnd));
        }catch (Exception e){
            project.setDateStart(null);
            project.setDateEnd(null);
        }
        project.setProjectId(UUID.randomUUID().toString());
        project.setStatus(Status.PLANNED);
        projectService.save(project);
        return "redirect:/projects";
    }
}
