package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.leonova.tm.enumerated.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
public final class Project implements Serializable {

    @Id
    @Column(name = "id")
    private String projectId;
    private String name;
    private String description;
    @Column(name = "createDate")
    private Date dateSystem;
    @Column(name = "beginEnd")
    private Date dateStart;
    @Column(name = "endDate")
    private Date dateEnd;

    @Column(name = "statusType")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Task> tasks;


}
