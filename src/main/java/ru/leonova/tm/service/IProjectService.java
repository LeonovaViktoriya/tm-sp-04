package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    Project findOneById(@NotNull String projectId) throws Exception;

    void save(@NotNull Project project);

    void removeProject(@NotNull Project project);

    void updateProject(@NotNull final String projectId, @NotNull final String projectName, @NotNull final String projectDesc,
                       @NotNull final Date dateStart,   @NotNull final Date dateEnd);
}
