package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService {
    @Transactional
    List<Task> findAll();

    Task findOneById(@NotNull String id) throws Exception;

    void removeTask(@NotNull Task task);

    void save(@NotNull Task task);

    List<Task> findAllByProjectId(@NotNull String id) throws Exception;

    void removeAllByProjectId(@NotNull String id) throws Exception;

    void updateTask(String name, String description, Date dateStart, Date dateEnd, String taskId);
}
