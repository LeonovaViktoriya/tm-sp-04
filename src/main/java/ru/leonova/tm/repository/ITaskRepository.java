package ru.leonova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.leonova.tm.entity.Task;

import java.util.Date;
import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {
    List<Task> findAllByProject_ProjectId(String id);

    void deleteAllByProject_ProjectId(String id);

    @Modifying
    @Query("update Task t set t.name=?1, t.description=?2, t.dateStart=?3, t.dateEnd=?4 where t.taskId= ?5")
    void updateTask(String name, String description, Date dateStart, Date dateEnd, String taskId);

}
