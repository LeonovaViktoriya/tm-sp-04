package ru.leonova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.leonova.tm.entity.Project;

import java.util.Date;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
    @Modifying
    @Query("update Project p set p.name = ?1, p.description = ?2, p.dateStart = ?3, p.dateEnd = ?4 where p.projectId = ?5")
    void updateProject(String name, String description, Date dateStart, Date dateEnd, String projectId);
}
