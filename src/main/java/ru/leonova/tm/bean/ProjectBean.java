package ru.leonova.tm.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.service.IProjectService;
import ru.leonova.tm.service.ITaskService;

import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ManagedBean(name = "projectBean")
@Named
@SessionScope
public class ProjectBean implements Serializable {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    private String projectId;
    private String name;
    private String description;
    private String dateStart;
    private String dateEnd;
    private String dateSystem;
    private String status;
    private List<Task> tasks;

    private final SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");

    public List<Project> findAllProject() {
        return projectService.findAll();
    }

    public Project findOneById(@NotNull final String projectId) throws Exception {
        return projectService.findOneById(projectId);
    }

    public void clearFields() throws Exception {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (!field.getName().equals("projectService") && !field.getName().equals("format"))
                ProjectBean.class.getDeclaredField(field.getName()).set(this, null);
        }
    }

    public String save(@NotNull final String name, String description, String startDate, String endDate) throws Exception {
        clearFields();
        @NotNull final Project project = new Project();
        project.setProjectId(UUID.randomUUID().toString());
        project.setName(name);
        project.setDescription(description);
        project.setStatus(Status.PLANNED);
        project.setDateStart(format.parse(startDate));
        project.setDateEnd(format.parse(endDate));
        project.setDateSystem(new Date());
        projectService.save(project);
        return "projectList?faces-redirect=true";
    }

    public String remove(@NotNull final Project project) throws Exception {
        taskService.removeAllByProjectId(project.getProjectId());
        projectService.removeProject(project);
        return "projectList?faces-redirect=true";
    }

    public String update(@NotNull final String projectId, @NotNull final String projectName, @NotNull final String projectDesc,
                         @NotNull final String dateStart,  @NotNull final String dateEnd) throws Exception {
        clearFields();
        projectService.updateProject(projectId, projectName, projectDesc, format.parse(dateStart), format.parse(dateEnd));
        return "projectList?faces-redirect=true";
    }

}
