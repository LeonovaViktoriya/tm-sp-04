package ru.leonova.tm.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.service.IProjectService;
import ru.leonova.tm.service.ITaskService;

import javax.inject.Named;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Named
@SessionScope
public class TaskBean implements Serializable {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    private String taskId;
    private String name;
    private String description;
    private String dateStart;
    private String dateEnd;
    private String dateSystem;
    private String status;

    private final SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");

    public List<Task> findAllByProjectId(@NotNull final String projectId) throws Exception {
        return taskService.findAllByProjectId(projectId);
    }

    public String save(@NotNull final String projectId, @NotNull final String name, String description, String dateStart, String dateEnd) throws Exception {
        @NotNull final Task task = new Task();
        task.setTaskId(UUID.randomUUID().toString());
        task.setProject(projectService.findOneById(projectId));
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(format.parse(dateStart));
        task.setDateEnd(format.parse(dateEnd));
        task.setDateSystem(new Date());
        task.setStatus(Status.PLANNED);
        taskService.save(task);
        return "projectList?faces-redirect=true";
    }

    public String remove(@NotNull final Task project) {
        taskService.removeTask(project);
        return "projectList?faces-redirect=true";
    }

    public String update(@NotNull final String taskId, @NotNull final String name, @NotNull final String description, @NotNull final String dateStart, @NotNull final String dateEnd) throws ParseException {
        taskService.updateTask(name, description, format.parse(dateStart), format.parse(dateEnd), taskId);
        return "projectList?faces-redirect=true";
    }

}
